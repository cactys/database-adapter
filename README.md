# database-adapter [![NPM version](https://img.shields.io/npm/v/database-adapter.svg)](https://www.npmjs.com/package/database-adapter) [![pipeline status](http://git.metaa.io/npm/database-adapter/badges/npm/pipeline.svg)](http://git.metaa.io/npm/database-adapter/commits/npm)
> An abstraction layer for database operation adapters

## Why?
Databases are a great thing. They come in many different ~~colors~~
shapes with a lot of different features but also, most importantly,
different query mechanisms and languages.

This module is supposed to make stuff easier (just like a lot of other
modules).  

## What?
Its goal is to define an abstraction layer you can implement your
database operations in without the need to worry about the hassle of
rewriting your code in case you should ever want to change the kind of
database you're using for your project.

> This module is still in an early stage and some parts of the tech
inside may still not be as configurable and cool as you'd want them to
be. If you're interested, please take your time to contribute to this
project!

(Other) developers (or you yourself) can create NPM modules that
function as peer dependencies which define database adapters using the
interface in here to make your code do what it's supposed to do.

One use-case could be reading the string-typed handle of database
adapters from a config file, allowing you to (pseudo-)hotswap your
database model by installing another adapter module and simply replacing
its key (and configuration) in your config file!

By default the database adapter tries to connect to the database,
validates its scheme against whatever you pass it in its configuration
object and then returns its object for you to use. In case something
goes wrong, it can try to reconnect or repair itself (which could also
just be creating the datastore for file-based database models).

## How?
This code is written in [TypeScript](https://www.typescriptlang.org/).
Using its typings and inheritance functionality allows you to define
nice adapters for various database models.

The source code of this module has an example for the awesome [LokiJS
datastore](http://lokijs.org/) attached (even though it also supports
adapters itself).

[- View example -](./src/example/index.ts)

I also turned that example adapter into an
[NPM module](https://www.npmjs.com/package/database-adapter-lokijs).

## Roadmap 
There's still stuff to do! Please do not hesitate to contribute!

*   Add abstract methods for default operations like inserting or
    querying data.
*   Add the plugin infrastructure mentioned above
*   Replace the example with tests

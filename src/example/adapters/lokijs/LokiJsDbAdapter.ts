import * as fs from 'fs'
import * as LokiJs from 'lokijs'
import * as touch from 'touch'
import { AbstractDatabaseAdapter } from '../../../lib'

import { ILokiJsDbOptions } from './ILokiJsDbOptions'


export class LokiJsDbAdapter extends AbstractDatabaseAdapter<Loki> {
	public static register(): string {
		return super.register('loki', this)
	}


	protected _config: ILokiJsDbOptions
	protected _connected: boolean = false

	public constructor(config: ILokiJsDbOptions) {
		super(config)
	}

	/**
	 * Returns the adapter's configuration.
	 * @returns {ILokiJsDbOptions}
	 */
	public get config(): ILokiJsDbOptions {
		return this._config
	}

	/**
	 * Returns the "connected" state.
	 * Connected means, that the database file was successfully opened.
	 * @returns {boolean}
	 */
	public get connected(): boolean {
		return this._connected
	}

	/**
	 * Returns the `Loki` object held by this adapter.
	 * @returns {Loki}
	 */
	public get raw(): Loki {
		return super.handle
	}

	/**
	 * Attempts to read the database file. Validates if there actually is a database in the configured path.
	 * @returns {Promise<void>}
	 */
	public async connect(): Promise<void> {
		if(fs.existsSync(this.config.path) && !fs.lstatSync(this.config.path).isFile()) {
			throw new Error(`Object in path "${this.config.path}" is not a file!`)
		}

		this.handle = new LokiJs(this.config.path.toString(), {
			serializationMethod: 'pretty',
		})

		return new Promise((resolve: ((_?: void) => any), reject: ((err: Error) => any)) => {
			fs.stat(this.config.path, (err: NodeJS.ErrnoException) => {
				if(err) return reject(new Error(`Database in path "${this.config.path}" does not exist!`))

				this.handle.loadDatabase({}, (err: Error) => {
					if(err) return reject(err)

					this._connected = true
					resolve()
				})
			})
		})
	}

	/**
	 * Creates the database file and fills it with the configured schema.
	 * @returns {Promise<void>}
	 */
	public async create(): Promise<void> {
		return touch<any>(this.config.path.toString())
			.then(err => {
				if(err) throw err

				if(this.config.schema) {
					for(let collection of this.config.schema) {
						this.handle.addCollection(collection)
					}
				}
				this.handle.save()
			})
	}

	/**
	 * Basically an alias for the create method.
	 * @returns {Promise<void>}
	 */
	public async repair(): Promise<void> {
		return this.create()
	}

	/**
	 * Reads all collections and compares them to the configured schema.
	 * @returns {Promise<void>}
	 * @throws {Error} When the collections don't match the configured schema.
	 */
	public async validate(): Promise<void> {
		if(!this.config.schema) return

		const missing: string[] = []

		for(let collection of this.config.schema) {
			if(!this.handle.getCollection(collection)) {
				missing.push(collection)
			}
		}

		if(missing.length) {
			throw new Error('Incomplete database schema.\nMissing collections:\n- ' + missing.join('\n- '))
		}
		return
	}

	/**
	 * Since LokiJS uses JSON files, we just attempt to create a new file here, using the `create` method.
	 * @returns {Promise<void>}
	 */
	public async handleUnreachable(): Promise<void> {
		return this.create()
	}

	/**
	 * Returns the database as a JSON object.
	 * @returns {any}
	 */
	public toJson(): any {
		return JSON.parse(this.handle.toJson())
	}
}

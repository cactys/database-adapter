import { IDatabaseAdapterInfo } from '../../../lib'
import { ILokiJsDbOptions } from './ILokiJsDbOptions'


export interface LokiJsDbOptions extends IDatabaseAdapterInfo<ILokiJsDbOptions> {
	type: 'loki',
	config: ILokiJsDbOptions
}

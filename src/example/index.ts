/**
 * This is an example in which we will use LokiJS to store cattle in our database.
 *
 * PLEASE NOTE:
 * We don't have abstract interfaces for database access like querying or inserting yet, so this example will use raw
 * functions of the LokiJS module.
 * Once abstract database calls are implemented, they will replace the raw calls!
 */

import { AbstractDatabaseAdapter, create } from '../lib'
/**
 * You would actually specify your plugin, e.g. "database-adapter-lokijs" in the following import, if you were to
 * hardcode the use of a plugin.
 */
import { ILokiJsDbOptions, LokiJsDbAdapter, LokiJsDbOptions } from './adapters/lokijs'


// for nice colored output
require('manakin').global


/**
 * This inserts a cow into our database. The gender will be random. There's gonna be milk if it's a female cow!
 * @param {AbstractDatabaseAdapter<any>} _dbAdapter
 * @returns {Promise<any>}
 */
const insertCow = (_dbAdapter: AbstractDatabaseAdapter<any>): Promise<any> => {
	const dbAdapter: LokiJsDbAdapter = <LokiJsDbAdapter> _dbAdapter

	return new Promise((resolve: (doc: Object) => any) => {
		let doc: any = {
			type: 'cow',
			utter: Math.random() < 0.5,
		}
		if(doc.utter) doc.milkVolume = Math.random()

		resolve(dbAdapter.raw.getCollection('moo').insert(doc))
	})
		.then(doc => {
			console.info('\nWe\'ve inserted this into the data store:')
			console.log(doc)

			return new Promise((resolve: () => any) => {
				dbAdapter.raw.save(err => {
					if(err) throw err
					resolve()
				})
			})
		})
}

const countCows = () => {
	create<Loki, ILokiJsDbOptions>(LokiJsDbAdapter, {
		path: 'example/example.json',
		schema: ['moo'],
	})
		.then((_dbAdapter: AbstractDatabaseAdapter<Loki>) => {
			const dbAdapter: LokiJsDbAdapter = <LokiJsDbAdapter> _dbAdapter,
				  queryA                     = {
					  type: 'cow',
				  },
				  queryB                     = {
					  type: 'cow',
					  utter: true,
				  },
				  moo                        = dbAdapter.raw.getCollection('moo')

			console.info(`We have ${moo.count(queryA)} cows in total of which ${moo.count(queryB)} can give milk!`)
		})
}

/**
 * This demonstrates all the different ways of initiating the adapter - sorted by the best and most specific way to the
 * worst. Obviously you should never write your code like in the following example, as it would just waste performance.
 *
 * Please note that the generic types used here only serve a purpose if you were to hard code usage of an adapter in
 * your TypeScript object. They do NOT have to be passed.
 */

/**
 * This section is needed for the plugins to load IN YOUR CODE, but it would break the example code, so we won't use it
 * here.
 */
//import * as dbAdapter from 'db-adapter'
//dbAdapter.loadPluginsSync()


create(LokiJsDbAdapter, {
	path: 'example/example.json',
	schema: ['moo'],
})
	.then(insertCow)
	.then(() => create<Loki, ILokiJsDbOptions>(LokiJsDbAdapter, {
		path: 'example/example.json',
		schema: ['moo'],
	}))
	.then(insertCow)
	.then(() => create<Loki, ILokiJsDbOptions>('loki', {
		path: 'example/example.json',
		schema: ['moo'],
	}))
	.then(insertCow)
	.then(() => create<Loki, LokiJsDbOptions>({
		type: 'loki',
		config: {
			path: 'example/example.json',
			schema: ['moo'],
		},
	}))
	.then(insertCow)
	.then(() => create<Loki>({
		type: 'loki',
		config: {
			path: 'example/example.json',
			schema: ['moo'],
		},
	}))
	.then(insertCow)
	.then(() => create({
		type: 'loki',
		config: {
			path: 'example/example.json',
			schema: ['moo'],
		},
	}))
	.then(insertCow)
	.then(() => {
		/**
		 * This should still be your preferred way that allows you to pass configuration and selection of the adapter in a
		 * configuration file or something similar.
		 */
		return create('loki', {
			path: 'example/example.json',
			schema: ['moo'],
		})
	})
	.then(insertCow)

	.then(() => {
		console.log()
		return countCows()
	})

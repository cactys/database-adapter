import { AbstractDatabaseAdapter } from '../AbstractDatabaseAdapter'
import { IDatabaseAdapterConfig } from './IDatabaseAdapterConfig'


export interface IDatabaseAdapterConstructor<T> {
	new<C extends IDatabaseAdapterConfig = IDatabaseAdapterConfig>(config: C): AbstractDatabaseAdapter<T>
}

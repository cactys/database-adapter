import { IDatabaseAdapterConfig } from './IDatabaseAdapterConfig'


export interface IDatabaseAdapterInfo<T extends IDatabaseAdapterConfig> {
	type: string,
	config: T
}
